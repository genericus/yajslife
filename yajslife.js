"use strict";

/**
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2013 Corey Wells
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

var board = [[]];
var scale = 20;

var animationInterval = null;

function clearCanvas(ctx, canvas)
{
    ctx.save();
    ctx.setTransform(1,0,0,1,0,0);
    ctx.clearRect(0,0,canvas.width,canvas.height);
    ctx.restore();
}

function getInputs ()
{
    // FIXME: This is pretty bad

    var newWidth = parseInt($("#gridWidth")[0].value, 10);
    var newHeight = parseInt($("#gridHeight")[0].value, 10);
    var interval = parseInt($("#animateInput")[0].value, 10);
    var newScale = parseInt($("#scaleInput")[0].value, 10);

    newWidth = Math.floor(newWidth);
    newHeight = Math.floor(newHeight);
    interval = Math.floor(interval);
    newScale = Math.floor(newScale);

    if (newWidth > 0 && newHeight > 0 &&
        interval >= 0 && newScale >= 1)
    {
        return [newWidth, newHeight, interval, newScale];
    }
    else
    {
        return undefined;
    }
}

function toggleAnimation ()
{
    if (getInputs() === undefined)
    {
        window.alert("Bad configuration");
    }
    else
    {
        if (animationInterval === null)
        {
            animationInterval = window.setInterval(selfDraw, getInputs()[2]);
        }
        else
        {
            window.clearInterval(animationInterval);
            animationInterval = null;
        }
    }
}

function applyConfig()
{
    var inputs = getInputs();
    if (animationInterval !== null)
    {
        window.clearInterval(animationInterval);
        animationInterval = null;
    }

    if (inputs)
    {
        board = makeGrid(inputs[0], inputs[1], 0);
        scale = inputs[3];
        canvas.width = board[0].length * scale;
        canvas.height = board.length * scale;

        selfDraw();
    }
    else
    {
        window.alert("Bad configuration");
    }
}

function drawGrid(ctx, canvas, grid, scale)
{
    clearCanvas(ctx, canvas);
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    scale = (scale !== null) ? scale : 10;

    for (var i = 0; i < grid.length; i++)
    {
        for (var j = 0; j < grid[i].length; j++)
        {
            if (grid[i][j] === 1)
            {
                ctx.fillStyle = "white";
                var x = (j * scale), y = (i * scale);

                ctx.fillRect(x, y, scale, scale);
            }
       }
   }
}

function makeGrid (width, height, fill)
{
    var grid = new Array(height);

    for (var i = 0; i < height; i++)
    {
        grid[i] = new Array(width);
        if (fill !== undefined)
        {
            for (var j = 0; j < width; j++)
            {
                grid[i][j] = fill;
            }
        }
    }

    return grid;
}

function copyGrid (oldGrid)
{
    // Thank you based jQuery
    return $.extend(true, [], oldGrid);
}

function getNumNeighbors (x, y, grid)
{
    // Return up to 8 neighbors at (x, y)
    // A cell would be grid[y][x]

    var total = 0;

    for (var i = (y - 1); i < (y + 2); i++)
    {
        if (grid[i] === undefined)
        {
            continue;
        }
        for (var j = (x - 1); j < (x + 2); j++)
        {
            // j is our x coord, i is our y coord
            if (grid[i][j] === undefined)
            {
                continue;
            }
            if (i === y && j === x)
            {
                continue;
            }
            if (grid[i][j] !== 0)
            {
                total++;
            }
        }
    }

    return total;
}

function generation (grid)
{
    var newGen = copyGrid(grid);

    for (var i = 0; i < grid.length; i++)
    {
        for (var j = 0; j < grid[i].length; j++)
        {
            // Any live cell with fewer than two live neighbours dies,
            // as if caused by under-population.  Any live cell with
            // two or three live neighbours lives on to the next
            // generation.  Any live cell with more than three live
            // neighbours dies, as if by overcrowding.  Any dead cell
            // with exactly three live neighbours becomes a live cell,
            // as if by reproduction.

            var alive = false;

            if (newGen[i][j] === 1)
            {
                alive = true;
            }

            var numNeigh = getNumNeighbors(j, i, grid);
            if (alive && numNeigh < 2)
            {
                newGen[i][j] = 0;
            }
            else if (alive && numNeigh > 3)
            {
                newGen[i][j] = 0;
            }
            else if (!alive && numNeigh === 3)
            {
                newGen[i][j] = 1;
            }
        }
    }

    return newGen;
}

function selfDraw()
{
    var canvas = $("#canvas")[0],
    ctx = canvas.getContext("2d");
    board = generation(board);
    drawGrid(ctx, canvas, board, scale);
}

function main()
{
    board = makeGrid(30, 30, 0);
    var canvas = $("#canvas")[0],
    ctx = canvas.getContext("2d");

    $("#configpanel").hide();

    $("#genButton").click(function () {
        selfDraw(); });
    $("#resetButton").click(applyConfig);
    $("#configToggleButton").click(function () {
        $("#configpanel").slideToggle("fast");
        });
    $("#animationButton").click(toggleAnimation);

//    applyConfig();
    canvas.width = board[0].length * scale;
    canvas.height = board.length * scale;

    $("#canvas").mousedown(function(e)
                           {
                               var offsetX = e.pageX - $(this).position().left;
                               var offsetY = e.pageY - $(this).position().top;

                               var clickedX = Math.floor(offsetX / scale);
                               var clickedY = Math.floor(offsetY / scale);

                               if (board[clickedY][clickedX] === 0)
                               {
                                   board[clickedY][clickedX] = 1;
                               }
                               else if (board[clickedY][clickedX] === 1)
                               {
                                   board[clickedY][clickedX] = 0;
                               }

                               drawGrid(ctx, canvas, board, scale);
                           });


    selfDraw();
}

window.onload = main;
