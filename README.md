yajslife
========

Yet Another JavaScript (Conway's Game of) Life. Uses `canvas` to draw the board.

Why?
----

I wanted to make it, regardless of whether there are hundreds of implementations just like this one (hint: there are).

This version is particularly bad in that it uses the most naive method possible to implement the game's rules: it checks all eight neighbors, for each cell. So it will check *hundreds* of times for a relatively small grid. Luckily we have fast processors nowadays. Have fun.

Then why put it up here?
------------------------

Because I made it myself and I'm proud of it, even with its flaws. I also wanted to play Game of Life with my own version.

What does it use?
-----------------

jQuery. Version 1 or 2, it does not matter.
